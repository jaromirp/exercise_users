<%@ page import="com.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: jarom
  Date: 25.09.2018
  Time: 18:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User list</title>
</head>
<body>
<%!
    public String generujLinkUsuwania(long id) {
        StringBuilder builder = new StringBuilder();
        builder.append("<a href=\"");
        builder.append("remove_user.jsp?user_id=" + id);
        builder.append("\">remove</a>");

        return builder.toString();

    }

    public String generujLinkModyfikacji(long id) {
        StringBuilder builder = new StringBuilder();
        builder.append("<a href=\"");
        builder.append("register_form.jsp?user_id=" + id);
        builder.append("\">modify</a>");

        return builder.toString();
    }

%>

<%
    List<User> userList;
    if (session.getAttribute("user_list") != null) {
        userList = (List<User>) session.getAttribute("user_list");
    } else {
        userList = new ArrayList<>();
    }
%>



<table>

    <thead>
    <th>Id</th>
    <th>Imie</th>
    </thead>


    <table style="width: 100%; border: solid 1px #000;">
        <thead>
        <th>Id</th>
        <th>first name</th>
        <th>last name</th>
        <th>birth date</th>
        <th>remove</th>
        <th>modify</th>
        </thead>

        <%
            for (User u : userList) {
                out.print("<tr>");
                out.print("<td>");
                out.print(u.getId());
                out.print("</td>");
                out.print("<td>");
                out.print(u.getFirstName());
                out.print("</td>");
                out.print("<td>");
                out.print(u.getLastName());
                out.print("</td>");
                out.print("<td>");
                out.print(u.getBirthDate());
                out.print("</td>");
                out.print("<td>");
                out.print(generujLinkModyfikacji(u.getId()));
                out.print("</td>");
                out.print("<td>");
                out.print(generujLinkUsuwania(u.getId()));
                out.print("</td>");
                //out.print(u);
                out.print("<tr/>");
            }

        %>
    </table>
</table>

<div>

    <form action="save_to_file.jsp">
        <input type="submit" value="save">
    </form>
    <form action="load_from_file.jsp">
        <input type="submit" value="load">
    </form>

</div>>

</body>
</html>
