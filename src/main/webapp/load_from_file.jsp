<%@ page import="java.io.BufferedReader" %>
<%@ page import="java.io.FileReader" %>
<%@ page import="com.User" %>
<%@ page import="java.io.FileNotFoundException" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: jarom
  Date: 28.09.2018
  Time: 19:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Load from file</title>

</head>
<body>

<%
    List<User> userList = new ArrayList<>();
    try (BufferedReader bufferedReader = new BufferedReader(new FileReader("data.txt"))) {
        String linia;
        while ((linia = bufferedReader.readLine()) != null) {
            User u = new User();
            u.loadFromSerializedLine(linia);

            userList.add(u);
        }
    } catch (FileNotFoundException fnfe) {
        out.print("Error");
    }
    session.setAttribute("user_list", userList);
    response.sendRedirect("user_list.jsp");

%>

</body>
</html>
