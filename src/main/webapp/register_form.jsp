
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.User" %>
<%@ page import="com.GENDER" %>

Created by IntelliJ IDEA.
  User: amen
  Date: 24.09.2018
  Time: 20:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Form</title>
</head>
<body>

<%
    ////////////////
    List<User> users;
    if (session.getAttribute("user_list") != null) { // sprawdzamy czy w sesji znajduje się nasza lista
        // wyciągamy listę userów z sesji jeśli tam jest
        users = (List<User>) session.getAttribute("user_list");
    } else {
        // jeśli listy nie ma w sesji tworzymy nową listę
        users = new ArrayList<>();
    }
    //////////////////

    String user_id = request.getParameter("user_id");

    User searched = null;
    boolean female = true;

    if (user_id != null) {
        Long modifiedId = Long.parseLong(user_id);
        Iterator<User> it = users.iterator();
        // dopóki mam następne elementy
        while (it.hasNext()) {
            // przechodzę do następnego elementu
            User user = it.next();

            // jeśli obecny element to ten, który mam usunąć
            if (user.getId() == modifiedId) {
                searched = user;
                break;
            }
        }

        if (searched != null) {
            female = searched.getGender() == GENDER.FEMALE;
        }
    }
%>

<form action="<%= searched== null ? "register_user.jsp" : "modify_user.jsp"%>" method="get">
    <input type="hidden" hidden value="<%= searched==null ? "" : searched.getId()%>" name="modified_id">
    <h2>Form:</h2>
    <div>
        <label for="firstName">First name:</label>
        <input id="firstName" name="firstName" type="text"
               value="<%= searched == null ? "" : searched.getFirstName() %>">
    </div>
    <div>
        <label for="lastName">Last name:</label>
        <input id="lastName" name="lastName" type="text" value="<%= searched == null ? "" :searched.getLastName() %>">
    </div>
    <div>
        <label for="username">Username:</label>
        <input id="username" name="username" type="text" value="<%= searched == null ? "" :searched.getUsername() %>">
    </div>
    <div>
        <label for="birthdate">Birth date:</label>
        <input id="birthdate" name="birthdate" type="datetime-local" value="<%= searched == null ? "" :searched.getBirthDate().toString() %>">
    </div>
    <div>
        <label for="address">Address:</label>
        <input id="address" name="address" type="text" value="<%= searched == null ? "" :searched.getAddress()%>">
    </div>
    <div>
        <label for="height">Height:</label>
        <input id="height" name="height" type="number" value="<%= searched == null ? "" :searched.getHeight() %>"
               min="60" max="230">
    </div>
    <%-- Gender = input type radio --%>
    <div>
        <label for="gender">Gender:</label>
        <input id="gender" name="gender" type="radio" value="MALE" <%= female ? "" : "checked" %> >MALE</input>
        <input name="gender" type="radio" value="FEMALE" <%= female ? "" : "checked" %> >FEMALE</input>
    </div>

    <input type="submit" value="Register">
</form>
</body>
</html>