
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page import="com.User" %>
<%@ page import="com.GENDER" %><%--
<%@ page import="com.javagda14.exercise.User" %>
<%@ page import="com.javagda14.exercise.GENDER" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.format.DateTimeFormatter" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 24.09.2018
  Time: 20:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User profile</title>
</head>
<body>
<%! int licznik = 0; %>

<%
    User u = new User();
    u.setId(licznik++); // 0, 1, 2...

    u.setFirstName(request.getParameter("firstName"));
    GENDER gender = GENDER.valueOf(request.getParameter("gender"));
    u.setGender(gender);
    u.setLastName(request.getParameter("lastName"));
    u.setAddress(request.getParameter("address"));
    u.setHeight(Integer.parseInt(wrequest.getParameter("height")));
    u.setUsername(request.getParameter("username"));
    u.setJoinDate(LocalDateTime.now());

    //2018-01-01T01:00
    LocalDateTime birthDate = LocalDateTime.parse(
            request.getParameter("birthdate"),
            DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm"));

    u.setBirthDate(birthDate);

    //////////////////////////////////////////////
    // walidacja wartości
    boolean isOk = true;
    if (!Character.isUpperCase(u.getFirstName().charAt(0))) {
        isOk = false;
    }
    if (!Character.isUpperCase(u.getLastName().charAt(0))) {
        isOk = false;
    }
    if (u.getHeight() < 60 || u.getHeight() > 230) {
        isOk = false;
    }
    if (u.getAddress().isEmpty()) {
        isOk = false;
    }
    if (u.getUsername().contains(" ")) {
        isOk = false;
    }

    if (!isOk) {
        response.sendRedirect("register_form.jsp" +
                "?firstName=" + request.getParameter("firstName") +
                "&lastName=" + request.getParameter("lastName") +
                "&height=" + request.getParameter("height"));
    }
%>
<div>
    <table>
        <thead>
        <th>Nazwa parametru</th>
        <th>Wartość</th>
        </thead>
        <tr>
            <td>Id:</td>
            <td>
                <%= u.getId() %>
            </td>
        </tr>
        <tr>
            <td>Username:</td>
            <td><%= u.getUsername()%>
            </td>
        </tr>
        <tr>
            <td>First name:</td>
            <td><%= u.getFirstName()%>
            </td>
        </tr>
        <tr>
            <td>Last name:</td>
            <td><%= u.getLastName()%>
            </td>
        </tr>
        <tr>
            <td>Gender:</td>
            <td><%= u.getGender()%>
            </td>
        </tr>
        <tr>
            <td>Height</td>
            <td><%= u.getHeight()%>
            </td>
        </tr>
        <tr>
            <td>Join date:</td>
            <td><%= u.getJoinDate()%>
            </td>
        </tr>
        <tr>
            <td>Birth date:</td>
            <td><%= u.getBirthDate()%>
            </td>
        </tr>
        <tr>
            <td>Address:</td>
            <td><%= u.getAddress()%>
            </td>
        </tr>
    </table>
</div>
</body>
</html>