<%@ page import="java.util.ArrayList" %>
<%@ page import="com.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %><%--
<%@ page import="com.javagda14.exercise.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 9/25/18
  Time: 6:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User list</title>
    <style>
        th {
            align-content: left;
        }
    </style>
</head>
<body>

<%
    List<User> users;
    if(session.getAttribute("user_list") != null){
        users = (List<User>) session.getAttribute("user_list");
    }else {
        users = new ArrayList<>();
    }

    String removeUserId = request.getParameter("user_id");
    Long removedId = Long.parseLong(removeUserId);

    Iterator<User> it =users.iterator();
    while (it.hasNext()){
        User user = it.next();

        if(user.getId() == removedId){
            it.remove();
            break;
        }
    }

    session.setAttribute("user_list", users);

    response.sendRedirect("user_list.jsp");

%>



<%--<%!--%>
    <%--public String generujLinkUsuwania(long id) {--%>
        <%--StringBuilder builder = new StringBuilder();--%>

        <%--builder.append("<a href=\"");--%>
        <%--builder.append("remove_user.jsp?user_id=" + id);--%>
        <%--builder.append("\">remove</a>");--%>

        <%--return builder.toString();--%>
    <%--}--%>

    <%--public String generujLinkModyfikacji(long id) {--%>
        <%--StringBuilder builder = new StringBuilder();--%>

        <%--builder.append("<a href=\"");--%>
        <%--builder.append("register_form.jsp?user_id=" + id);--%>
        <%--builder.append("\">modify</a>");--%>

        <%--return builder.toString();--%>
    <%--}--%>
<%--%>--%>

<%--<%--%>
    <%--List<User> userList;--%>
    <%--if (session.getAttribute("user_list") != null) {--%>
        <%--userList = (List<User>) session.getAttribute("user_list");--%>
    <%--} else {--%>
        <%--userList = new ArrayList<>();--%>
    <%--}--%>
<%--%>--%>

<%--<table style="width: 100%; border: solid 1px #000;">--%>
    <%--<thead>--%>
    <%--<th>Id</th>--%>
    <%--<th>Imie</th>--%>
    <%--<th>Nazwisko</th>--%>
    <%--<th>Data urodzenia</th>--%>
    <%--<th></th>--%>
    <%--<th></th>--%>
    <%--</thead>--%>
    <%--<%--%>
        <%--for (User u : userList) {--%>
            <%--out.print("<tr>");--%>
            <%--out.print("<td>");--%>
            <%--out.print(u.getId());--%>
            <%--out.print("</td>");--%>
            <%--out.print("<td>");--%>
            <%--out.print(u.getFirstName());--%>
            <%--out.print("</td>");--%>
            <%--out.print("<td>");--%>
            <%--out.print(u.getLastName());--%>
            <%--out.print("</td>");--%>
            <%--out.print("<td>");--%>
            <%--out.print(u.getBirthDate());--%>
            <%--out.print("</td>");--%>
            <%--out.print("<td>");--%>
            <%--out.print(generujLinkUsuwania(u.getId()));--%>
            <%--out.print("</td>");--%>
            <%--out.print("<td>");--%>
            <%--out.print(generujLinkModyfikacji(u.getId()));--%>
            <%--out.print("</td>");--%>
            <%--out.print("</tr>");--%>
        <%--}--%>
    <%--%>--%>
<%--</table>--%>

</body>
</html>
